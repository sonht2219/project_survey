﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectSurvey.Models
{
    public class ResponeApi<T>
    {
        public int Status { get; set; }
        public string Message { get; set; }

        public T Data { get; set; }
    }
}
