﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectSurvey.Models
{
    public class Point
    {
        public int Id { get; set; }
        [ForeignKey("User")]
        public int UserId { get; set; }
        public virtual User User { get; set; }
        [ForeignKey("Survey")]
        public int SurveyId { get; set; }
        public virtual Survey Survey { get; set; }
        public int TotalScore { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public PointStatus Status { get; set; }

        public Point()
        {
            CreatedAt = DateTime.Now;
            UpdatedAt = DateTime.Now;
            Status = PointStatus.Activated;
        }

    }

    public enum PointStatus
    {
        Activated = 1,
        Deactivated = -1
    }
}
