﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectSurvey.Models
{
    public class Question
    {
        public int Id { get; set; }
        public string QuestionText { get; set; }
        [ForeignKey("Survey")]
        public int SurveyId { get; set; }
        public string Answer { get; set; } 
        public virtual Survey Survey { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public QuestionStatus Status { get; set; }

        public Question()
        {
            CreatedAt = DateTime.Now;
            UpdatedAt = DateTime.Now;
            Status = QuestionStatus.Activated;
        }
    }

    public enum QuestionStatus
    {
        Activated = 1,
        Deactivated = -1
    }
}
