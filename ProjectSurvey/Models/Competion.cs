﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectSurvey.Models
{
    public class Competion
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string BigImage { get; set; }
        public string SmallImage { get; set; }
        public string Address { get; set; }
        public DateTime InTime { get; set; }
        public string Organizer { get; set; }
        public string Link { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public CompetionStatus Status { get; set; }

        public Competion()
        {
            CreatedAt = DateTime.Now;
            UpdatedAt = DateTime.Now;
            Status = CompetionStatus.Activated;
        }

    }

    public enum CompetionStatus
    {
        Activated = 1,
        Deactivated = -1
    }
}
