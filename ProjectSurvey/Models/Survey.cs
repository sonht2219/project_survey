﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectSurvey.Models
{
    public class Survey
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public SurveyFor SurveyFor { get; set; }
        public DateTime End { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public SurveyStatus Status { get; set; }

        public Survey()
        {
            CreatedAt = DateTime.Now;
            UpdatedAt = DateTime.Now;
            Status = SurveyStatus.Activated;
            SurveyFor = SurveyFor.Student;
        }
    }
    public enum SurveyStatus
    {
        Activated = 1,
        Deactivated = -1
    }

    public enum SurveyFor
    {
        Student = 1,
        Employee = -1
    }
}
