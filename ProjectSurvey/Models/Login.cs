﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectSurvey.Models
{
    public class Login
    {
        public int ID { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }

        public Login()
        {
        }

        public Login(string username, string password)
        {
            Username = username;
            Password = password;
        }
    }
}
