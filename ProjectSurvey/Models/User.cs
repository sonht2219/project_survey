﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectSurvey.Models
{
    public class User
    {
        public int ID { get; set; }
        public string Username { get; set; }
        
        [JsonIgnore]
        public string Password { get; set; }
        public int Role { get; set; }
        public int Status { get; set; }
        public string Name { get; set; }
        public string NumberID { get; set; }
        public string Job { get; set; }
        public string Class { get; set; }
        public string Section { get; set; }
        public DateTime TimeJoin { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        [NotMapped]
        public string AccessToken { get; set; }


        public User()
        {
            this.CreatedAt = DateTime.Now;
            this.UpdatedAt = DateTime.Now;
            this.Role = 0;
            this.Status = -1;
        }

        public User(string username, string password, string name, string numberID, string job, string @class, string section, DateTime timeJoin, string email, string phone)
        {
            Username = username;
            Password = BCrypt.Net.BCrypt.HashPassword(password);
            Role = 0;
            Status = -1;
            Name = name;
            NumberID = numberID;
            Job = job;
            Class = @class;
            Section = section;
            TimeJoin = timeJoin;
            Email = email;
            Phone = phone;
            CreatedAt = DateTime.Now;
            UpdatedAt = DateTime.Now;

        }


    }
}
