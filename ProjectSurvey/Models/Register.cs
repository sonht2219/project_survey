﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectSurvey.Models
{
    public class Register
    {
        [Required]
        [StringLength(100, MinimumLength = 6)]
        public string Username { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 6)]
        public string Password { get; set; }
        public int Role { get; set; }
        public int Status { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string NumberID { get; set; }
        [Required]
        public string Job { get; set; }
        public string Class { get; set; }
        public string Section { get; set; }
        [Required]
        public DateTime TimeJoin { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        public User GetUser()
        {
            return new User(Username, Password, Name, NumberID, Job, Class, Section, TimeJoin, Email, Phone);
        }
    }
}
