﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjectSurvey.Models;

namespace ProjectSurvey.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuestionsController : ControllerBase
    {
        private readonly ProjectSurveyContext _context;

        public QuestionsController(ProjectSurveyContext context)
        {
            _context = context;
        }

        // GET: api/Questions
        [HttpGet]
        public IEnumerable<Question> GetQuestion()
        {
            return _context.Question.Where(q=> q.Status == QuestionStatus.Activated);
        }
        [HttpGet("Users/{userid}/Surveys/{surveyid}")]
        public IQueryable<Point> GetUserInSurvey(int userid , int surveyid)
        {
            var data =   _context.Point.Where(q=>q.UserId == userid && q.SurveyId ==surveyid);
            
            if (data.Count() < 1  )
            {
               Console.WriteLine("123");
                
            }
            else
            {
               
            }
            return data;
        }
//        return _context.Survey.Where(q => (search != null && (q.Title.Contains(search) && q.Status == SurveyStatus.Activated)


        //
        [HttpGet("Surveys/{id}")]
        [Produces(typeof(Question))]
        public async Task<IActionResult> GetBySurvey(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var question = _context.Question.Where(q => q.SurveyId == id && (q.Status == QuestionStatus.Activated)).Include(p => p.Survey);

            if (question.Count() < 1  )
            {
                return NotFound("Not found");
            }
            else
            {
                return Ok(question);
            }
        }

        // GET: api/Questions/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetQuestion([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
           
            var question = await _context.Question.FindAsync(id);
          
            if (question == null  && question.Status == QuestionStatus.Deactivated)
            {
                return NotFound();
            }

            return Ok(question);
        }

        // PUT: api/Questions/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutQuestion([FromRoute] int id, [FromBody] Question question)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != question.Id)
            {
                return BadRequest();
            }

            _context.Entry(question).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!QuestionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Questions
        [HttpPost]
        public async Task<IActionResult> PostQuestion([FromBody] Question question)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Question.Add(question);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetQuestion", new { id = question.Id }, question);
        }

        // DELETE: api/Questions/5
//        [HttpDelete("{id}")]
//        public async Task<IActionResult> DeleteQuestion([FromRoute] int id)
//        {
//            if (!ModelState.IsValid)
//            {
//                return BadRequest(ModelState);
//            }
//
//            var question = await _context.Question.FindAsync(id);
//            if (question == null)
//            {
//                return NotFound();
//            }
//
//            _context.Question.Remove(question);
//            await _context.SaveChangesAsync();
//
//            return Ok(question);
//        }

        private bool QuestionExists(int id)
        {
            return _context.Question.Any(e => e.Id == id);
        }
    }
}