﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjectSurvey.Models;

namespace ProjectSurvey.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompetionsController : ControllerBase
    {
        private readonly ProjectSurveyContext _context;

        public CompetionsController(ProjectSurveyContext context)
        {
            _context = context;
        }

        // GET: api/Competions
        [HttpGet]
        public IEnumerable<Competion> GetCompetion()
        {

            return _context.Competion;
        }

        // GET: api/Competions/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCompetion([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var competion = await _context.Competion.FindAsync(id);

            if (competion == null)
            {
                return NotFound();
            }

            return Ok(competion);
        }

//        // PUT: api/Competions/5
//        [HttpPut("{id}")]
//        public async Task<IActionResult> PutCompetion([FromRoute] int id, [FromBody] Competion competion)
//        {
//            if (!ModelState.IsValid)
//            {
//                return BadRequest(ModelState);
//            }
//
//            if (id != competion.Id)
//            {
//                return BadRequest();
//            }
//
//            _context.Entry(competion).State = EntityState.Modified;
//
//            try
//            {
//                await _context.SaveChangesAsync();
//            }
//            catch (DbUpdateConcurrencyException)
//            {
//                if (!CompetionExists(id))
//                {
//                    return NotFound();
//                }
//                else
//                {
//                    throw;
//                }
//            }
//
//            return NoContent();
//        }
//
//        // POST: api/Competions
//        [HttpPost]
//        public async Task<IActionResult> PostCompetion([FromBody] Competion competion)
//        {
//            if (!ModelState.IsValid)
//            {
//                return BadRequest(ModelState);
//            }
//
//            _context.Competion.Add(competion);
//            await _context.SaveChangesAsync();
//
//            return CreatedAtAction("GetCompetion", new { id = competion.Id }, competion);
//        }
//
//        // DELETE: api/Competions/5
//        [HttpDelete("{id}")]
//        public async Task<IActionResult> DeleteCompetion([FromRoute] int id)
//        {
//            if (!ModelState.IsValid)
//            {
//                return BadRequest(ModelState);
//            }
//
//            var competion = await _context.Competion.FindAsync(id);
//            if (competion == null)
//            {
//                return NotFound();
//            }
//
//            _context.Competion.Remove(competion);
//            await _context.SaveChangesAsync();
//
//            return Ok(competion);
//        }

        private bool CompetionExists(int id)
        {
            return _context.Competion.Any(e => e.Id == id);
        }
    }
}