﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjectSurvey.Models;

namespace ProjectSurvey.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FaqsController : ControllerBase
    {
        private readonly ProjectSurveyContext _context;

        public FaqsController(ProjectSurveyContext context)
        {
            _context = context;
        }

        // GET: api/Faqs
        [HttpGet]
        public IEnumerable<Faq> GetFaq()
        {
            return _context.Faq;
        }

        // GET: api/Faqs/5
//        [HttpGet("{id}")]
//        public async Task<IActionResult> GetFaq([FromRoute] int id)
//        {
//            if (!ModelState.IsValid)
//            {
//                return BadRequest(ModelState);
//            }
//
//            var faq = await _context.Faq.FindAsync(id);
//
//            if (faq == null)
//            {
//                return NotFound();
//            }
//
//            return Ok(faq);
//        }

        // PUT: api/Faqs/5
//        [HttpPut("{id}")]
//        public async Task<IActionResult> PutFaq([FromRoute] int id, [FromBody] Faq faq)
//        {
//            if (!ModelState.IsValid)
//            {
//                return BadRequest(ModelState);
//            }
//
//            if (id != faq.Id)
//            {
//                return BadRequest();
//            }
//
//            _context.Entry(faq).State = EntityState.Modified;
//
//            try
//            {
//                await _context.SaveChangesAsync();
//            }
//            catch (DbUpdateConcurrencyException)
//            {
//                if (!FaqExists(id))
//                {
//                    return NotFound();
//                }
//                else
//                {
//                    throw;
//                }
//            }
//
//            return NoContent();
//        }
//
//        // POST: api/Faqs
//        [HttpPost]
//        public async Task<IActionResult> PostFaq([FromBody] Faq faq)
//        {
//            if (!ModelState.IsValid)
//            {
//                return BadRequest(ModelState);
//            }
//
//            _context.Faq.Add(faq);
//            await _context.SaveChangesAsync();
//
//            return CreatedAtAction("GetFaq", new { id = faq.Id }, faq);
//        }
//
//        // DELETE: api/Faqs/5
//        [HttpDelete("{id}")]
//        public async Task<IActionResult> DeleteFaq([FromRoute] int id)
//        {
//            if (!ModelState.IsValid)
//            {
//                return BadRequest(ModelState);
//            }
//
//            var faq = await _context.Faq.FindAsync(id);
//            if (faq == null)
//            {
//                return NotFound();
//            }
//
//            _context.Faq.Remove(faq);
//            await _context.SaveChangesAsync();
//
//            return Ok(faq);
//        }

        private bool FaqExists(int id)
        {
            return _context.Faq.Any(e => e.Id == id);
        }
    }
}