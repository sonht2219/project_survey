﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjectSurvey.Models;

namespace ProjectSurvey.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SurveysController : ControllerBase
    {
        private readonly ProjectSurveyContext _context;

        public SurveysController(ProjectSurveyContext context)
        {
            _context = context;
        }

        // GET: api/Surveys
        [HttpGet]
        public IEnumerable<Survey> GetSurvey([FromQuery]string search = null)
        {
            //            return _context.Survey;
            if (search == null)
            {
                return _context.Survey;
            }
            return _context.Survey.Where(q => (search != null && (q.Title.Contains(search) && q.Status == SurveyStatus.Activated)
                )
            );
        }
        [HttpGet("Role")]
        public IEnumerable<Survey> GetByRole(int id)
        {

            var idUser = AccountID();
            User user = _context.User.SingleOrDefault(i => i.ID == idUser);
            if (user.Role == 2)
            {
                return _context.Survey.Where(q => q.SurveyFor == SurveyFor.Student && (q.Status == SurveyStatus.Activated));
            }
            return _context.Survey.Where(q => q.SurveyFor == SurveyFor.Employee && (q.Status == SurveyStatus.Activated));

        }


        // GET: api/Surveys/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSurvey([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var survey = await _context.Survey.FindAsync(id);

            if (survey == null)
            {
                return NotFound();
            }

            return Ok(survey);
        }

        // PUT: api/Surveys/5
//        [HttpPut("{id}")]
//        public async Task<IActionResult> PutSurvey([FromRoute] int id, [FromBody] Survey survey)
//        {
//            if (!ModelState.IsValid)
//            {
//                return BadRequest(ModelState);
//            }
//
//            if (id != survey.Id)
//            {
//                return BadRequest();
//            }
//
//            _context.Entry(survey).State = EntityState.Modified;
//
//            try
//            {
//                await _context.SaveChangesAsync();
//            }
//            catch (DbUpdateConcurrencyException)
//            {
//                if (!SurveyExists(id))
//                {
//                    return NotFound();
//                }
//                else
//                {
//                    throw;
//                }
//            }
//
//            return NoContent();
//        }

        // POST: api/Surveys
//        [HttpPost]
//        public async Task<IActionResult> PostSurvey([FromBody] Survey survey)
//        {
//            if (!ModelState.IsValid)
//            {
//                return BadRequest(ModelState);
//            }
//
//            _context.Survey.Add(survey);
//            await _context.SaveChangesAsync();
//
//            return CreatedAtAction("GetSurvey", new { id = survey.Id }, survey);
//        }

        // DELETE: api/Surveys/5
//        [HttpDelete("{id}")]
//        public async Task<IActionResult> DeleteSurvey([FromRoute] int id)
//        {
//            if (!ModelState.IsValid)
//            {
//                return BadRequest(ModelState);
//            }
//
//            var survey = await _context.Survey.FindAsync(id);
//            if (survey == null)
//            {
//                return NotFound();
//            }
//
//            _context.Survey.Remove(survey);
//            await _context.SaveChangesAsync();
//
//            return Ok(survey);
//        }

        private bool SurveyExists(int id)
        {
            return _context.Survey.Any(e => e.Id == id);
        }

        protected int AccountID()
        {
            return int.Parse(this.User.Claims.First(i => i.Type == "UserID").Value);
        }

    }
}