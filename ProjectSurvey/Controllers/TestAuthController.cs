﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using ProjectSurvey.Models;

namespace ProjectSurvey.Controllers
{
    [Authorize]
    [Route("api/auth")]
    [ApiController]
    public class TestAuthController : ControllerBase
    {
        private readonly ProjectSurveyContext _context;
        private readonly IMemoryCache _memoryCache;

        public TestAuthController(ProjectSurveyContext context)
        {
            _context = context;
        }

        [HttpGet("userdata")]
        public User Test()
        {
            var id = int.Parse(this.User.Claims.First(i => i.Type == "UserID").Value);
            return _context.User.SingleOrDefault(i => i.ID == id);


        }

        [HttpGet("user-data")]
        protected User AccountID()
        {
            var id = int.Parse(this.User.Claims.First(i => i.Type == "UserID").Value);
            return _context.User.SingleOrDefault(i => i.ID == id);
        }

    }
}