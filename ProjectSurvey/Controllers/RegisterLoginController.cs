﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ProjectSurvey.Models;
using ProjectSurvey.Utility;

namespace ProjectSurvey.Controllers
{
    [Route("api")]
    [ApiController]
    public class RegisterLoginController : ControllerBase
    {
        private readonly ProjectSurveyContext _context;
        private readonly AppSettings _appSettings;
        private readonly string DOMAIN_SURVEY = "https://projectsurvey20190122034118.azurewebsites.net";

        public RegisterLoginController(ProjectSurveyContext context, IOptions<AppSettings> appSettings)
        {
            _context = context;
            _appSettings = appSettings.Value;
        }

        [HttpPost("register")]
        [EnableCors("MyPolicy")]
        public async Task<IActionResult> Register([FromBody] Register register)
        {
            if (AccountExists(register.Username))
            {
                return StatusCode(400, new { status = 400, message = "Username existed" });
            }
            User user = register.GetUser();
            _context.User.Add(user);
            await _context.SaveChangesAsync();

            return StatusCode(201, new { status = 201, message = "created success", data = user});
        }

        [HttpPost("login")]
        [EnableCors("MyPolicy")]
        public async Task<IActionResult> Login([FromBody] Login login)
        {
            User user =  _context.User.SingleOrDefault(a => a.Username == login.Username);
            if (user == null || !BCrypt.Net.BCrypt.Verify(login.Password, user.Password))
            {
                return StatusCode(400, new { status = 400, message = "Username or Password incorrect" });
            }
            if (user.Status==-1)
            {
                return StatusCode(400, new { status = 400, message = "Please wait confirm admin" });
            }

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Issuer = DOMAIN_SURVEY,
                Audience = DOMAIN_SURVEY,
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim("UserID", user.ID.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.AccessToken = tokenHandler.WriteToken(token);

            return StatusCode(200, new { status = 200, message = "Login Success", data = user });
        } 

        private bool AccountExists(string username)
        {
            return _context.User.Any(e => e.Username == username);
        }
    }
}