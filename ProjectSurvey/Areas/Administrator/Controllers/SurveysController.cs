﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

using ProjectSurvey.Models;

namespace ProjectSurvey.Areas.Administrator.Controllers
{
    [Area("Administrator")]
    public class SurveysController : Controller
    {
        private readonly ProjectSurveyContext _context;


        public SurveysController(ProjectSurveyContext context)
        {
            _context = context;
        }

        // GET: Administrator/Surveys
        public async Task<IActionResult> Index()
        { 
           return View(await _context.Survey.ToListAsync());
        }
        //
      

        // GET: Administrator/Surveys/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var survey = await _context.Survey
                .FirstOrDefaultAsync(m => m.Id == id);
            if (survey == null)
            {
                return NotFound();
            }

            return View(survey);
        }

        // GET: Administrator/Surveys/Create
        public IActionResult Create()
        {

            return View();
        }

        // POST: Administrator/Surveys/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,Description,Image,SurveyFor,End,CreatedAt,UpdatedAt,Status")] Survey survey)
        {
            if (ModelState.IsValid)
            {
                _context.Add(survey);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(survey);
        }

        // GET: Administrator/Surveys/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var survey = await _context.Survey.FindAsync(id);
            if (survey == null)
            {
                return NotFound();
            }
            return View(survey);
        }

        // POST: Administrator/Surveys/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int? id, [Bind("Id,Title,Description,Image,SurveyFor,End,Status")] Survey survey)
        {           
            if (!_context.Survey.Any(e => e.Id == id))
            {
                return NotFound();
            }
            var existSurvey = await _context.Survey.FindAsync(id);
            if (existSurvey == null)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                try
                {
                    existSurvey.Title = survey.Title;
                    existSurvey.Description = survey.Description;
                    existSurvey.Image = survey.Image;
                    existSurvey.SurveyFor = survey.SurveyFor;
                    existSurvey.Status = survey.Status;
                    existSurvey.End = survey.End;
                    existSurvey.UpdatedAt = DateTime.Now;
                    _context.Update(existSurvey);
                    _context.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
                return RedirectToAction(nameof(Index));
            }
            return View(survey);
        }

        // GET: Administrator/Surveys/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var survey = await _context.Survey
                .FirstOrDefaultAsync(m => m.Id == id);
            if (survey == null)
            {
                return NotFound();
            }

            return View(survey);
        }

        // POST: Administrator/Surveys/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var survey = await _context.Survey.FindAsync(id);
            _context.Survey.Remove(survey);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SurveyExists(int id)
        {
            return _context.Survey.Any(e => e.Id == id);
        }
    }
}
