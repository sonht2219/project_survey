﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProjectSurvey.Models;

namespace ProjectSurvey.Areas.Administrator.Controllers
{
    [Area("Administrator")]
    public class CompetionsController : Controller
    {
        private readonly ProjectSurveyContext _context;

        public CompetionsController(ProjectSurveyContext context)
        {
            _context = context;
        }

        // GET: Administrator/Competions
        public async Task<IActionResult> Index()
        {
            return View(await _context.Competion.ToListAsync());
        }

        // GET: Administrator/Competions/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var competion = await _context.Competion
                .FirstOrDefaultAsync(m => m.Id == id);
            if (competion == null)
            {
                return NotFound();
            }

            return View(competion);
        }

        // GET: Administrator/Competions/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Administrator/Competions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,Description,BigImage,SmallImage,Address,InTime,Organizer,Link,CreatedAt,UpdatedAt,Status")] Competion competion)
        {
            if (ModelState.IsValid)
            {
                _context.Add(competion);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(competion);
        }

        // GET: Administrator/Competions/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var competion = await _context.Competion.FindAsync(id);
            if (competion == null)
            {
                return NotFound();
            }
            return View(competion);
        }

        // POST: Administrator/Competions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,Description,BigImage,SmallImage,Address,InTime,Organizer,Link,CreatedAt,UpdatedAt,Status")] Competion competion)
        {
            if (id != competion.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(competion);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CompetionExists(competion.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(competion);
        }

        // GET: Administrator/Competions/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var competion = await _context.Competion
                .FirstOrDefaultAsync(m => m.Id == id);
            if (competion == null)
            {
                return NotFound();
            }

            return View(competion);
        }

        // POST: Administrator/Competions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var competion = await _context.Competion.FindAsync(id);
            _context.Competion.Remove(competion);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CompetionExists(int id)
        {
            return _context.Competion.Any(e => e.Id == id);
        }
    }
}
