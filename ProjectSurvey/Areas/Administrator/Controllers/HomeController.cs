﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjectSurvey.Models;

namespace ProjectSurvey.Areas.Administrator.Controllers
{
    [Area("Administrator")]
    public class HomeController : Controller
    {
        private readonly ProjectSurveyContext _context;

        public HomeController(ProjectSurveyContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> Index()
        {

            return View("Index");
        }

    }
}