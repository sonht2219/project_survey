﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProjectSurvey.Models;

namespace ProjectSurvey.Areas.Administrator.Controllers
{
    [Area("Administrator")]
    public class PointsController : Controller
    {
        private readonly ProjectSurveyContext _context;

        public PointsController(ProjectSurveyContext context)
        {
            _context = context;
        }

        // GET: Administrator/Points
        public async Task<IActionResult> Index()
        {
            var projectSurveyContext = _context.Point.Include(p => p.Survey).Include(p => p.User);
            return View(await projectSurveyContext.ToListAsync());
        }

        public async Task<IActionResult> GetBySurvey(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var point = _context.Point.Where(q => q.SurveyId == id && (q.Status == PointStatus.Activated)).Include(p => p.Survey).Include(p => p.User);

            if (point == null)
            {
                return NotFound("Not found");
            }
            else
            {
                return View(point);
            }
        }

        // GET: Administrator/Points/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var point = await _context.Point
                .Include(p => p.Survey)
                .Include(p => p.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (point == null)
            {
                return NotFound();
            }

            return View(point);
        }

        // GET: Administrator/Points/Create
        public IActionResult Create()
        {
            ViewData["SurveyId"] = new SelectList(_context.Survey, "Id", "Id");
            ViewData["UserId"] = new SelectList(_context.User, "ID", "ID");
            return View();
        }

        // POST: Administrator/Points/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,UserId,SurveyId,TotalScore,CreatedAt,UpdatedAt,Status")] Point point)
        {
            if (ModelState.IsValid)
            {
                _context.Add(point);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["SurveyId"] = new SelectList(_context.Survey, "Id", "Id", point.SurveyId);
            ViewData["UserId"] = new SelectList(_context.User, "ID", "ID", point.UserId);
            return View(point);
        }

        // GET: Administrator/Points/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var point = await _context.Point.FindAsync(id);
            if (point == null)
            {
                return NotFound();
            }
            ViewData["SurveyId"] = new SelectList(_context.Survey, "Id", "Id", point.SurveyId);
            ViewData["UserId"] = new SelectList(_context.User, "ID", "ID", point.UserId);
            return View(point);
        }

        // POST: Administrator/Points/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,UserId,SurveyId,TotalScore,CreatedAt,UpdatedAt,Status")] Point point)
        {
            if (id != point.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(point);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PointExists(point.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["SurveyId"] = new SelectList(_context.Survey, "Id", "Id", point.SurveyId);
            ViewData["UserId"] = new SelectList(_context.User, "ID", "ID", point.UserId);
            return View(point);
        }

        // GET: Administrator/Points/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var point = await _context.Point
                .Include(p => p.Survey)
                .Include(p => p.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (point == null)
            {
                return NotFound();
            }

            return View(point);
        }

        // POST: Administrator/Points/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var point = await _context.Point.FindAsync(id);
            _context.Point.Remove(point);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PointExists(int id)
        {
            return _context.Point.Any(e => e.Id == id);
        }
    }
}
