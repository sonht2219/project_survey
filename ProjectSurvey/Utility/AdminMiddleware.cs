﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ProjectSurvey.Models;

namespace ProjectSurvey.Utility
{
    public class AdminMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IMemoryCache _memoryCache;
        private readonly ILogger<Startup> _logger;

        public AdminMiddleware(RequestDelegate next, ILogger<Startup> logger, IMemoryCache memoryCache)
        {
            _next = next;
            _logger = logger;
            _memoryCache = memoryCache;
        }

        public async Task Invoke(HttpContext context)
        {
            if (!context.Request.Path.ToString().ToLower().Contains("/administrator") || context.Request.Path.ToString().ToLower().Contains("/administrator/logins"))
            {
                await _next.Invoke(context);
                return;
            }

            var token = context.Request.Cookies["access_token"];
            if (token == null)
            {
                context.Response.Redirect("/administrator/logins");
                return;
            }

            if (!_memoryCache.TryGetValue(token, out User user))
            {
                using (HttpClient client = new HttpClient())
                {
                    try
                    {
                        client.DefaultRequestHeaders.Add("authorization", "Bearer " + token);
                        HttpResponseMessage response = await client.GetAsync("https://projectsurvey20190122034118.azurewebsites.net/api/auth/userdata");
                        response.EnsureSuccessStatusCode();
                        string responseBody = await response.Content.ReadAsStringAsync();

                        user = JsonConvert.DeserializeObject<User>(responseBody);

                        var cacheEntryOption = new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(45));
                        _memoryCache.Set(token, user, cacheEntryOption);
                    }
                    catch (HttpRequestException e)
                    {
                        _logger.LogError(e.Message);
                        context.Response.Redirect("/administrator/logins");
                        return;
                    }
                }
            }
            await _next.Invoke(context);

        }
    }
}
