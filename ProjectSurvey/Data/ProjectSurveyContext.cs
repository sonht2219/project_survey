﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectSurvey.Models;

namespace ProjectSurvey.Models
{
    public class ProjectSurveyContext : DbContext
    {
        public ProjectSurveyContext (DbContextOptions<ProjectSurveyContext> options)
            : base(options)
        {
        }

        public DbSet<ProjectSurvey.Models.Survey> Survey { get; set; }
        public DbSet<ProjectSurvey.Models.User> User { get; set; }
        public DbSet<ProjectSurvey.Models.Question> Question { get; set; }
        public DbSet<ProjectSurvey.Models.Point> Point { get; set; }
        public DbSet<ProjectSurvey.Models.Faq> Faq { get; set; }
        public DbSet<ProjectSurvey.Models.Competion> Competion { get; set; }
        public DbSet<ProjectSurvey.Models.Login> Login { get; set; }

    }
}
